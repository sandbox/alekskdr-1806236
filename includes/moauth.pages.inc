<?php

/**
* @file
* Administrative pages and functions for Mail.ru OAuth module.
*/

/**
* Menu callback; Display the settings form for Mail.ru authentification.
*/
function moauth_settings_form($form, &$form_state) {
  if (!function_exists('curl_init')) {
    $warning = '<p>'
             . t('Please enable <a href="@link" target="_blank">CURL library</a>.',
                 array('@link' => 'http://php.net/manual/en/book.curl.php'))
             . '</p>';
  }

  $disabled = (isset($warning)) ? TRUE : FALSE;
  if ($disabled) {
    variable_del('moauth_app_id');
  }

  $registered = variable_get('moauth_app_id', 0)
             && variable_get('moauth_secret_key', '')
             && variable_get('moauth_private_key', '');

  $form['moauth_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#size' => 20,
    '#maxlengh' => 50,
    '#description' => t('To use Mail.ru connect, a Mail.ru Application must be created. ')
              . t('Set up your app in <a href="http://api.mail.ru/sites/my/" target="_blank">my apps</a> on Mail.ru. ')
              . t('Enter your App ID here.'),
    '#default_value' => variable_get('moauth_app_id', ''),
  );

  $form['moauth_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('App Secret key'),
    '#size' => 40,
    '#maxlengh' => 100,
    '#description' => t('To use Mail.ru connect, a Mail.ru Application must be created. ')
                    . t('Set up your app in <a href="http://api.mail.ru/sites/my/" target="_blank">my apps</a> on Mail.ru. ')
                    . t('Enter your App Secret key here.'),
    '#default_value' => variable_get('moauth_secret_key', ''),
  );

  $form['moauth_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('App Private key'),
    '#size' => 40,
    '#maxlengh' => 100,
    '#description' => t('To use Mail.ru connect, a Mail.ru Application must be created. ')
                    . t('Set up your app in <a href="http://api.mail.ru/sites/my/" target="_blank">my apps</a> on Mail.ru. ')
                    . t('Enter your App Private key here.'),
    '#default_value' => variable_get('moauth_private_key', ''),
  );
  
  $form['moauth_basic_mapping'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic mapping'),
  );

  $form['moauth_basic_mapping']['moauth_user_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Import Mail.ru e-mail address'),
    '#description' => '',
    '#default_value' => variable_get('moauth_user_email', TRUE),
  );

  $user_pictures = variable_get('user_pictures', 0);
  if (!function_exists('curl_init')) {
    $user_pictures = FALSE;
    drupal_set_message(t('You can\'t use mail.ru avatars because your PHP don\'t support <a href="@curl">CURL functions</a>.',
                         array('@curl' => url('http://php.net/curl'))),
                       'error');
  }

  $form['moauth_basic_mapping']['moauth_user_picture'] = array(
    '#type' => 'select',
    '#title' => t('Import Mail.ru user picture'),
    '#description' => t('You should enable user pictures on <a href="@usp">user settings page</a>.',
                        array('@usp' => url('admin/config/people/accounts'))),
    '#default_value' => variable_get('moauth_user_picture', 0),
    '#options' => array(
      '0' => t('Don\'t use'),
      'pic_small' => t('Use small avatar 45x45'),
      'pic' => t('Use medium avatar 90x90'),
      'pic_big' => t('Use big avatar'),
    ),
    '#disabled' => !$user_pictures,
  );
  

  $form['moauth_basic_mapping']['moauth_user_username'] = array(
    '#type' => 'radios',
    '#title' => t('User name import'),
    '#options' => array(
      'nick' => t('Mail.ru nickname'),
      'login' => t('Mail.ru login'),
    ),
    '#description' => t('Select the Mail.ru value used to set the user\'s name when connecting with Mail.ru.'),
    '#default_value' => variable_get('moauth_user_username', 'nick'),
  );

  // Add in Field module mapping options, which are always available.
  $form['moauth_user_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('User field mapping'),
    '#description' => t("Each of your <a href=\"@url\">fields that are attached to users</a> are listed below. ",
                        array('@url' => url('admin/config/people/accounts/fields')))
                    . t('Map each one you would like to import into your site to a Mail.ru data source.'),
    '#tree' => TRUE,
    '#weight' => 5,
  );

  // Each field type can only map to certain type Mail.ru properties. Build a
  // list for each type that includes reasonable options.
  $properties = moauth_user_properties();
  $property_options = array();
  foreach ($properties as $property => $property_info) {
    if (isset($property_info['field_types'])) {
      foreach ($property_info['field_types'] as $field_type) {
        $property_options[$field_type][$property] = '[' . $property . '] ' . $property_info['label'];
      }
    }
  }

  $field_defaults = variable_get('moauth_user_fields', array());
  $instances = field_info_instances('user', 'user');
  foreach ($instances as $field_name => $instance) {
    $field = field_info_field($instance['field_name']);
    if (isset($property_options[$field['type']])) {
      $options = array_merge(array('' => t('- Do not import -')),
                             $property_options[$field['type']]);
      $form['moauth_user_fields'][$field_name] = array(
        '#title' => t($instance['label']),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($field_defaults[$field_name]) ? $field_defaults[$field_name] : '',
      );
    } else {
      $form['moauth_user_fields'][$field_name] = array(
        '#title' => t($instance['label']),
        '#type' => 'form_element',
        '#children' => '<em>' . t('No mappable Mail.ru properties.') . '</em>',
        '#theme_wrappers' => array('form_element'),
      );
    }
  }

  // Add the role to the user.
  $roles = array();
  $result = db_query("SELECT * FROM {role} WHERE rid > 2 ORDER by name");
  foreach($result as $role) {
    $roles[$role->rid] = $role->name;
  }

  $form['moauth_user_role'] = array(
    '#type' => 'fieldset',
    '#title' => t('User role mapping'),
    '#description' => t('You can define roles for new users Mail.ru. ')
                    . t('If you need to <a href="@url">add new roles</a>.',
                        array('@url' => url('admin/people/permissions/roles'))),
    '#tree' => TRUE,
    '#weight' => 10,
  );

  $form['moauth_user_role']['moauth_role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Role for new Mail.ru user'),
    '#options' => $roles,
    '#default_value' => variable_get('moauth_role', array()),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
* Form validation function for moauth_settings_form().
*/
function moauth_settings_form_validate($form, &$form_state) {
  // Remove trailing spaces from keys.
  $form_state['values']['moauth_app_id']      = trim($form_state['values']['moauth_app_id']);
  $form_state['values']['moauth_secret_key']  = trim($form_state['values']['moauth_secret_key']);
  $form_state['values']['moauth_private_key'] = trim($form_state['values']['moauth_private_key']);

  // Do some basic data input validation.
  if (!is_numeric($form_state['values']['moauth_app_id'])) {
    form_error($form['moauth_app_id'],
               t('The App ID must be an integer.'));
  }  
  if (strlen($form_state['values']['moauth_secret_key']) != 32) {
    form_error($form['moauth_secret_key'],
               t('The App Secret does not appear to be valid. It is usually a 32 character hash.'));
  }
  if (strlen($form_state['values']['moauth_private_key']) != 32) {
    form_error($form['moauth_private_key'],
               t('The App Secret does not appear to be valid. It is usually a 32 character hash.'));
  }
}

/**
* Form submission function for moauth_settings_form().
*/
function moauth_settings_form_submit($form, &$form_state) {
  variable_set('moauth_app_id', $form_state['values']['moauth_app_id']);
  variable_set('moauth_secret_key', $form_state['values']['moauth_secret_key']);
  variable_set('moauth_private_key', $form_state['values']['moauth_private_key']);

  variable_set('moauth_user_email', $form_state['values']['moauth_user_email']);
  variable_set('moauth_user_username', $form_state['values']['moauth_user_username']);
  variable_set('moauth_user_picture', $form_state['values']['moauth_user_picture']);

  variable_set('moauth_role', array_filter($form_state['values']['moauth_user_role']['moauth_role']));

  moauth_field_form_submit($form, $form_state);

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
* Return a list of Mail.ru user properties.
*
* This function provides a list of properties that may be attached directly to
* a Mail.ru user account. This information is immediately available when a
* user logs in with Mail.ruk connect and may be stored locally.
*
* The returned array of properties provides a human-readable name
* for the property.
*/
function moauth_user_properties() {
  $properties = array(
    'nick' => array(
      'label' => t('Mail.ru nick'),
      'field_types' => array('text'),
    ),
    'first_name' => array(
      'label' => t('First name'),
      'field_types' => array('text'),
    ),
    'last_name' => array(
      'label' => t('Last name'),
      'field_types' => array('text'),
    ),
    'gender' => array(
      'label' => t('Gender'),
      'field_types' => array('list_integer'),
    ),
    'birthday' => array(
      'label' => t('Birthday'),
      'field_types' => array('text'),
    ),
    'link' => array(
      'label' => t('My world@Mail.ru profile link'),
      'field_types' => array('text'),
    ),
    'country' => array(
      'label' => t('Country'),
      'field_types' => array('text'),
    ),
    'region' => array(
      'label' => t('Region'),
      'field_types' => array('text'),
    ),
    'city' => array(
      'label' => t('City'),
      'field_types' => array('text'),
    ),
  );

  drupal_alter('moauth_user_properties', $properties);

  return $properties;
}

/**
* Submit handler for the Mail.ru authentification settings form.
*/
function moauth_field_form_submit(&$form, &$form_state) {
  if (isset($form_state['values']['moauth_user_fields'])) {
    variable_set('moauth_user_fields',
                 array_filter($form_state['values']['moauth_user_fields']));
  }
}