<?php

/**
* @file
* Functions to import information from Mail.ru, registration and authorization
* users
*/

/**
* Get and staticaly cache Mail.ru IDs and user IDs of Drupal site.
*
* @param string $option
*   The ID you want to get:
*   - uid: Returns Drupal user ID from Mail.ru ID.
*   - muid: Returns Mail.ru ID from Drupal ID.
* @param int $id
*   A Drupal uid or Mail.ru ID.
*
* @return int
*/
function moauth_get_id($option = '', $id = NULL) {
  switch ($option) {
    case 'uid':
      $muid = $id;
      if (isset($muid)) {
        $uid = db_query(
          'SELECT uid FROM {moauth_users} WHERE muid = :muid',
          array(':muid' => $muid)
        )->fetchField();
        if ($uid) {
          $result_id = $uid;
        }
        else {
          $result_id = 0;
        }
      }
      else {
        $result_id = 0;
      }
      break;

    case 'muid':
      $uid = isset($id) ? $id : $GLOBALS['user']->uid;
      if (isset($uid)) {
        $muid = db_query(
          'SELECT muid FROM {moauth_users} WHERE uid = :uid',
          array(':uid' => $uid)
        )->fetchField();
        if ($muid) {
          $result_id = $muid;
        }
        else {
          $result_id = 0;
        }
      }
      else {
        $result_id = 0;
      }
      break;

    default:
      $result_id = 0;
      break;
  }
  
  return $result_id;
}

/**
* Implementation of REST API method users.getInfo().
*
* @param int $muid
*   A Mail.ru ID.
*
* @todo details on the API: http://api.mail.ru/docs/reference/rest/users.getInfo
*/
function moauth_users_getinfo($muid) {
  $moauth_app_id =      variable_get('moauth_app_id', 0);
  $moauth_secret_key =  variable_get('moauth_secret_key', '');
  $moauth_private_key = variable_get('moauth_private_key', '');
  include_once './includes/common.inc';

  $data = array();
  parse_str(urldecode($_COOKIE['mrc']), $data);

  $path = 'http://www.appsmail.ru/platform/api';

  $method = 'method=users.getInfo';
  $app_id = 'app_id=' . $moauth_app_id;
  $session_key = 'session_key=' . $data['session_key'];
  $secure = 'secure=1';
  $uids = 'uids=' . $muid;

  $params = $app_id . $method . $secure . $session_key . $uids;
  $sid_to_encode = $params . $moauth_secret_key;
  $sig = md5($sid_to_encode);

  // Format the URL query Mail.ru
  $api_uri = sprintf('%s&%s&%s&%s&%s&sig=%s',
                     $method,
                     $app_id,
                     $session_key,
                     $secure,
                     $uids,
                     $sig);
  $uri = $path . '?' . $api_uri;
  $api_result = drupal_http_request($uri);
  if (isset($api_result->data)) {
    $jdec = json_decode($api_result->data, TRUE);

    $res = isset($jdec[0]) ? $jdec[0] : NULL;
    $state = array();
    if (is_array($res) && isset($res['error'])) {
      drupal_set_message(t($res['error']['error_msg']), 'error');
      if (user_access('administer moauth')) {
        if ($res['error']['error_code'] == 104) {
          drupal_set_message(t('Check your settings !url',
                     array('!url' => l(
                              t('here'), 'admin/config/people/moauth'
                              ))),
                     'error');
        }
      }

      return FALSE;

    }
    elseif ($res['link']) {
      $state = $res;

      $url = parse_url($state['link']);
      $url['path'] = trim($url['path'], "/");
      $args = explode("/", $url['path']);
      $state['host'] = $args[0];
      $state['login'] = $args[1];
      if ($state['host'] == 'corp') {
        $state['host'] = 'corp.mail';
        $state['email'] = sprintf('%s@%s.ru', $state['login'], $state['host']);
      }
      if (isset($state['location']['country']['name'])) {
        $state['country'] = $state['location']['country']['name'];
      }
      else {
        $state['country'] = '';
      }
      if (isset($state['location']['region']['name'])) {
        $state['region'] = $state['location']['region']['name'];
      }
      else {
        $state['region'] = '';
      }
      if (isset($state['location']['region']['name'])) {
        $state['region'] = $state['location']['region']['name'];
      }
      else {
        $state['region'] = '';
      }
      $state['gender'] = $state['sex'];
      return $state;
    }
  }

  return FALSE;
}

/**
* Given a Drupal user object, log the user in.
*
* @param $account
*   A Drupal user account or UID.
*/
function moauth_login_user($account) {
  if ($account->status) {
    $form_state['uid'] = $account->uid;
    user_login_submit(array(), $form_state);
    return TRUE;
  }
  else {
    drupal_set_message(t('The username %name has not been activated or is blocked.', array('%name' => $account->name)), 'error');
    return FALSE;
  }
}

/**
* Finalize Mail.ru login for new user
*/
function moauth_success() {
  global $user;

  $uid = 0;
  $GLOBALS['conf']['cache'] = false;

  // If you have access to the information on Mail.ru
  if (isset($_COOKIE['mrc'])) {
    $data = array();
    parse_str(urldecode($_COOKIE['mrc']), $data);

    if (isset($data['vid'])) {
      $uid = moauth_get_id('uid', $data['vid']);
    }

    if ($uid && !$user->uid && ($account = user_load($uid))) {
      // Instant login of user and goto destination
      moauth_login_user($account);
      drupal_goto('<front>');
    }
    else {
      // Get user information with Mail.ru 
      $state = moauth_users_getinfo($data['vid']);
      if (is_array($state)) {
        if ($user->uid) {
          // First login then make corrections to account
          moauth_user_update_set($user, $state);
          drupal_set_message(
            t("You've connected your account with Mail.ru.")
          );
          drupal_goto('user/' . $user->uid);
        }
        // If the Mail.ruk e-mail address matches an existing account,
        // bind them together and log in as that account.
        elseif (!empty($state['email'])
          && ($account = user_load_by_mail($state['email']))
        ) {
          moauth_save($account->uid, $state['uid']);
          // Logins will be denied if the user's account is blocked.
          if (moauth_login_user($account)) {
            drupal_set_message(
              t("You've connected your account with Mail.ru.")
            );
            drupal_goto('user/' . $account->uid);
          }
          else {
            drupal_goto('<front>');
            return '';
          }
        }
        else {
          // Create new Drupal user of the Mail.ru
          $account = moauth_new_user_set($state);
          moauth_login_user($account);
          drupal_goto('user/' . $account->uid . '/edit');
          return '';
        }
      }
      else {
        drupal_set_message(t('Error getting data from Mail.ru'), 'error');
        drupal_goto('<front>');
        return '';
      }
    }
  }
  else {
    drupal_set_message(t('Please, login via Mail.ru first.'), 'error');
    return '<div id="moauth_login" class="moauth_button"></div>'
         . moauth_page_bottom_js();
  }
}

/**
* Remove the integration with Mail.ru
*/
function moauth_delete_integration() {
  global $user;

  // Checking session_key
  if (isset($_REQUEST['moauth_action']) && $user->uid != 0) {
    $skey = md5(session_id() . 'isdbYGyu34389GYe3Z0csn');
    if (strtoupper($_REQUEST['moauth_action']) == strtoupper($skey)) {
      $muid = moauth_get_id('muid', $user->uid);
      if (!empty($muid)) {
        // Delete the existing Mail.ru ID if present for this Drupal user.
        $delete_query = 'DELETE FROM {moauth_users} WHERE uid = :uid';
        db_query($delete_query, array(':uid' => $user->uid));
        drupal_set_message(t('Removed integration with Mail.ru'), 'status');
      }
      else {
        drupal_set_message(t('No integration with Mail.ru'), 'error');
      }
    }
    else {
      drupal_set_message(t('Does not match the security key'), 'error');
    }
  }
  else {
    drupal_set_message(t('No security key'), 'error');
  }

  drupal_goto('user/' . $user->uid . '/edit');
}

/**
* Given a Mail.ru user object, associate or save a Drupal user account.
*
* @param object $muser
*   A Drupal user
* @param array $options
*   A User data
*
* @return object
*   The user object to modify or add.
*/
function moauth_user_update_set($muser, $options = array()) {
  // Set default options.
  $defaults = array(
    'picture' => (
                  variable_get('user_pictures', 0)
                  && variable_get('moauth_user_picture', 0)
                 ) ? 1 : 0,
  );
  
  $options = array_merge($options, $defaults);

  // Initialize basic properties that are unlikely to need changing.
  if (variable_get('moauth_user_email', 0)) {
    $edit = array(
      'mail' => !empty($options['email']) ? $options['email'] : '',
      'init' => !empty($options['email']) ? $options['email'] : '',
    );
  }

  moauth_field_create_user($edit, $options);

  // Set user roles
  moauth_set_roles($edit, $muser);

  $account = user_save($muser, $edit);

  // Retrieve the user's picture from Mail.ru and save it locally.
  if ($account->uid && $options['picture'] == 1 && $options['has_pic'] == 1) {
    $picture_directory = file_default_scheme()
                       . '://'
                       . variable_get('user_picture_path', 'pictures');
    if (file_prepare_directory($picture_directory, FILE_CREATE_DIRECTORY)) {
      // Select url image from Mail.ru
      $img_size = variable_get('moauth_user_picture', 0);
      $image_url = $options[$img_size];
      $image_save_name = $picture_directory . '/picture-' . $account->uid . '-' . REQUEST_TIME . '.jpg';
      
      $picture_result = drupal_http_request($image_url);
      if(is_object($picture_result) && !empty($picture_result->data)){
        $picture_path = file_stream_wrapper_uri_normalize($image_save_name);
        $picture_file = file_save_data($picture_result->data, $picture_path, FILE_EXISTS_REPLACE);

        // Check to make sure the picture isn't too large for the site settings.
        $max_dimensions = variable_get('user_picture_dimensions', '90x90');
        file_validate_image_resolution($picture_file, $max_dimensions);

        // Update the user record.
        $picture_file->uid = $account->uid;
        $picture_file = file_save($picture_file);
        file_usage_add($picture_file, 'user', 'user', $account->uid);
        db_update('users')
          ->fields(array(
          'picture' => $picture_file->fid,
          ))
          ->condition('uid', $account->uid)
          ->execute();
      }
    }
  }

  moauth_save($account->uid, $options['uid']);
}

/**
* Given a Mail.ru user data, create a Drupal user account.
*
* @param array $options
*   A User data
*
* @return object
*   The user object to add.
*/
function moauth_new_user_set($options) {
  // Set default options.
  $defaults = array(
    'picture' => (
                  variable_get('user_pictures', 0)
                  && variable_get('moauth_user_picture', 0)
                 ) ? 1 : 0,
    'status' => variable_get('user_register', 1) == 1 ? 1 : 0,
  );

  $options = array_merge($options, $defaults);

  $edit = array();

  $username = variable_get('moauth_user_username', 'nick');
  $login = explode('@', $options['email']);
  $login = $login[0];
  $nick = !empty($options['nick']) ? $options['nick'] : $login;
  $username = $username === 'nick' ? $nick : $login;

  $query = "SELECT uid FROM {users} WHERE name = :name";
  $uid = db_query($query, array(':name' => $username))->fetchField();
  $i = 0;
  while ($uid) {
    $i++;
    $uid = db_query($query, array(':name' => ($username . $i)))->fetchField();
  }
  if ($i > 0) {
    $username = $username . $i;
  }

  // Set new email
  $flag = variable_get('moauth_user_email', 0);
  $_mail = $flag ? $options['email'] : '';
  $_init = $flag ? $options['email'] : '';

  // Initialize basic properties that are unlikely to need changing.
  $edit = array(
    'name' => $username,
    'mail' => $_mail,
    'init' => $_init,
    // If user_register is "1", then no approval required.
    'status' => $options['status'],
    'timezone' => variable_get('date_default_timezone'),
  );
  // Initialize the additional fields
  moauth_field_create_user($edit, $options);
  // Set user roles
  moauth_set_roles($edit);

  // Create new user
  $account = user_save(NULL, $edit);

  moauth_save($account->uid, $options['uid']);

  // Retrieve the user's picture from Mail.ru and save it locally.
  if ($account->uid && $options['picture'] == 1 && $options['has_pic'] == 1) {
    $picture_directory = file_default_scheme()
                       . '://'
                       . variable_get('user_picture_path', 'pictures');
    if (file_prepare_directory($picture_directory, FILE_CREATE_DIRECTORY)) {
      // Select url image from Mail.ru
      $img_size = variable_get('moauth_user_picture', 0);
      $image_url = $options[$img_size];
      $image_save_name = $picture_directory . '/picture-' . $account->uid . '-' . REQUEST_TIME . '.jpg';

      $picture_result = drupal_http_request($image_url);
      if(is_object($picture_result) && !empty($picture_result->data)){
        $picture_path = file_stream_wrapper_uri_normalize($image_save_name);
        $picture_file = file_save_data($picture_result->data, $picture_path, FILE_EXISTS_REPLACE);

        // Check to make sure the picture isn't too large for the site settings.
        $max_dimensions = variable_get('user_picture_dimensions', '90x90');
        file_validate_image_resolution($picture_file, $max_dimensions);

        // Update the user record.
        $picture_file->uid = $account->uid;
        $picture_file = file_save($picture_file);
        file_usage_add($picture_file, 'user', 'user', $account->uid);
        db_update('users')
          ->fields(array(
          'picture' => $picture_file->fid,
          ))
          ->condition('uid', $account->uid)
          ->execute();
      }
    }
  }

  return $account;
}

/**
* Add field info to a Drupal user array (before account creation).
*
* @param array $edit
*   User data are passed by reference
* @param array $options
*   The data obtained from the Mail.ru
*/
function moauth_field_create_user(&$edit, $options) {
  $field_map = variable_get('moauth_user_fields', array());
  $instances = field_info_instances('user', 'user');

  foreach ($instances as $field_name => $instance) {
    if (isset($field_map[$field_name])) {
      $mailru_property_name = $field_map[$field_name];
      if (array_key_exists($mailru_property_name, $options)) {
        $edit[$field_name][LANGUAGE_NONE][0]['value'] = $options[$mailru_property_name];
      }
    }
  }
}

/**
* Set roles for the user Mail.ru
* 
* @param array $edit
*   Array of data for user to save
* @param object $account
*   User object
*/
function moauth_set_roles(&$edit, $account = NULL) {
  $roles = array();
  $uroles = array();
  $result = db_query("SELECT * FROM {role} WHERE rid > 2");
  foreach($result as $record) {
    $roles[$record->rid] = $record->name;
  }
  $rs = variable_get('moauth_role', 0);
  if (is_array($rs)) {
    foreach($rs as $rid) {
      if ($rid) {
        $uroles[$rid] = $roles[$rid];
      }
    }
    if (is_array($uroles)) {
      if (!empty($account) && is_array($account->roles)) {
        $edit['roles'] = $account->roles + $uroles;
      }
      else {
        $edit['roles'] = $uroles;
      }
    }
  }
}

/**
* Save a Drupal User ID to Mail.ru ID pairing.
*
* Passing in NULL for $muid can also be used to delete a UID to MUID pairing.
*
* @param int $uid
*   A Drupal uid
* @param int $muid
*   A Mail.ru ID
*/
function moauth_save($uid, $muid) {
  // Delete the existing Mail.ru ID if present for this Drupal user.
  $delete_query = 'DELETE FROM {moauth_users} WHERE uid = :uid';
  
  // If setting a new Mail.ru ID for an account, also make sure no other
  // Drupal account is connected with this Mail.ru ID.
  if (isset($muid)) {
    $delete_query .= ' OR muid = :muid';
    db_query($delete_query, array(':uid' => $uid, ':muid' => $muid));
  }
  else {
    db_query($delete_query, array(':uid' => $uid));
  }
  
  if (!empty($muid)) {
    $id = db_insert('moauth_users')
    ->fields(array('uid' => $uid, 'muid' => $muid))
    ->execute();
  }
}

/**
* Return js code for button Mail.ru
*/
function moauth_page_bottom_js() {
  global $base_url;

  $destination = drupal_get_destination();

  // Get application settings
  $moauth_app_id      = variable_get('moauth_app_id', 0);
  $moauth_secret_key  = variable_get('moauth_secret_key', '');
  $moauth_private_key = variable_get('moauth_private_key', '');
  
  // Application settings are set
  $page_bottom = '<script type="text/javascript" src="http://cdn.connect.mail.ru/js/loader.js"></script>'
       . "<script type=\"text/javascript\">
        //<![CDATA[
        jQuery(document).ready(function($){
          mailru.loader.require('api', function() {
          // initializes the internal variables
          mailru.connect.init(" . $moauth_app_id . ", '" . $moauth_private_key . "');
          // register event handlers that are invoked at login and logout
          mailru.events.listen(mailru.connect.events.login, function(session){
            document.cookie = 'moauth=login; path=/';
            window.location = '" . $base_url . "/moauth/success?" . $destination['destination'] . "';
          });
          mailru.events.listen(mailru.connect.events.logout, function(){
            document.cookie = 'moauth=logout; path=/';
            window.location.reload();
          });
          $('<a class=\"mrc__connectButton\">" . t('login@mail.ru') . "</a>').appendTo('.moauth_button');
          mailru.connect.initButton();
          });
        });
        //]]>
        </script>";

  return $page_bottom;
}