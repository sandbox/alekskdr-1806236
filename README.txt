Mail.ru - is Russian national email service.
Statistics shows up to 60%-70% user registrations using emails from Mail.ru service

Mail.ru oauth module allow users to enter your site with account on mail.ru

FEATURES
---------
Automatic detection of emails of registered users
Ability to assign multiple roles for new users
Synchronizing the avatar on first user login

INSTALL
------------------

1. Download module from drupal.org
2. Copy files to your modules directory. Often its 'sites/all/modules'
3. Go to admin/modules page and enable Mail.ru oauth module
4. Now you have to register your web-site
   Go to http://api.mail.ru/sites/my/add/
   You have to be logged at http://my.mail.ru
   Accept user agreement.
   Enter title of your site and front page url

   After registration you've got ID, Secret key and Private key

   You have to enter these values at the module settings page.

6. Go to admin/config/people/moauth
   enter your ID, Secret key and Private key
   Login button will be displayed only if you have entered all 3 values.

7. You can enable the Mail.ru connect button either by enabling the included
   Login from Mail.ru block under "Structure" -> "Blocks", or you can print out the
   link manually in your theme with the following code:
   
   <div id="moauth_login" class="moauth_button"></div>
   <div id="js_warning_moauth">
      Please enable javascript to login via mail.ru
   </div>


DEPENDENCIES
------------

cURL http://php.net/manual/en/book.curl.php
drupal_http_request()
JavaScript
Cookies
